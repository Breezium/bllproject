using System;
using System.Collections.Generic;
using UnityEngine;

public class Levels : MonoBehaviour
{
    static Levels sLevels = null;
    public static Levels GetLevels() { return sLevels; }

    public List<LevelDefinitions> levelDefinitions;
    public int currentLevel;

    // Start is called before the first frame update
    void Awake()
    {
        if (sLevels != null)
        {
            Destroy(gameObject);
            return;
        }
        sLevels = this;

        DontDestroyOnLoad(gameObject);
    }

    internal LevelDefinitions GetCurrentLevel()
    {
        return levelDefinitions[currentLevel];
    }

    internal void IncrementLevel()
    {
        currentLevel = (currentLevel+1) % levelDefinitions.Count;
    }
}
