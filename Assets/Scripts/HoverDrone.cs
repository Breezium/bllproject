using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HoverDrone : MonoBehaviour
{
    public Vector3 direction;
    public float distance =0;
    public float rate = 0;
    public bool isMoving;
    internal Vector3 exitPoint;
    bool levelFinished;

    public void Hover(Vector3 dir, float dist)
    {
        Hover(dir, dist, rate);
    }

    public void Hover(Vector3 dir, float dist, float _rate)
    {
        direction = dir;
        distance = dist;
        rate = _rate;
     
        if(!isMoving && !levelFinished)
            StartCoroutine(MoveDrone());
    }

    private IEnumerator MoveDrone()
    {
        this.isMoving = true;
        Vector3 start = transform.position;
        isMoving = true;
        float startTime = Time.time;
        while (isMoving)
        {
            float pingpong = Mathf.Clamp01((Time.time - startTime) * rate);
            Vector3 peak = start + direction.normalized * distance;
            float factor = pingpong * pingpong * (3f - 2f * pingpong);

            transform.position = Vector3.Lerp(start, peak, factor);
            Quaternion endRotation = Quaternion.LookRotation(direction.normalized, Vector3.back);
            transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, factor);
            yield return null;
            if (pingpong >= 1f)
                isMoving = false;
        }

        if ((transform.position - exitPoint).sqrMagnitude < 1f) 
        {
            //pretty much there
            levelFinished = true;
            Levels.GetLevels().IncrementLevel();
            Invoke("Reload", 3f);
        }
    }

    public void Reload()
    {
        SceneManager.LoadScene(0);
    }
}
