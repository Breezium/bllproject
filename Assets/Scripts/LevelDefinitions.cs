using System.Collections.Generic;

[System.Serializable]
public class LevelDefinitions 
{
    public int xSize;
    public int ySize;
    public float cellSize;
    public List<GameElementPosition> gameElementList;//define population of level.
}
