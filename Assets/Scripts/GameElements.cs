using System.Collections.Generic;
using UnityEngine;

public enum eGameElementType
{
    SECURITYCAMERA,
    EXITPOINT,
    STARTPOINT,
    DEFAULT,
};

[System.Serializable]
public class GameElementPosition
{
    public eGameElementType eType;
    public int positionIndex;
}

[System.Serializable]
public class GameElementModel
{
    public eGameElementType eType;
    public GameObject prefabObject;
    public bool obstacle;
}

public class GameElements : MonoBehaviour
{
    public List<GameElementModel> gameElement;
}
