using System;
using System.Collections.Generic;
using UnityEngine;
/*

public class LevelDefinitions
{
    public int xSize;
    public int ySize;
    public float cellSize;
    public List<GameElementPosition> gameElementList;//define population of level.
}
*/

[RequireComponent(typeof(GameElements))]
public class Grid : MonoBehaviour
{
    static Grid sGrid = null;
    public static Grid GetGrid() { return sGrid; }
    
    public GameObject tilePrefab;
    public HoverDrone myDrone;
    public float droneRange = 1f;

    private int xSize, ySize;
    private float cellSize;
    private List<GameElementPosition> gameElementList;//define population of level.
    private Vector3[] posArray;
    private bool[] availabilityArray;
    private Vector3 gridOffset;
    private GameElements gameElements;

    private void Awake()
    {
        if (sGrid)
        {
            Destroy(gameObject);
            return;
        }
        sGrid = this;

        gameElements = GetComponent<GameElements>();
        
    }

    private void Start()
    {
        Initialise();
    }

    public void Initialise()
    {
        LevelDefinitions levelDef = Levels.GetLevels().GetCurrentLevel();
            
        xSize = levelDef.xSize;
        ySize = levelDef.ySize;
        cellSize = levelDef.cellSize;
        gameElementList = levelDef.gameElementList;
        droneRange = levelDef.cellSize;

        posArray = new Vector3[xSize * ySize];
        availabilityArray = new bool[xSize * ySize];
        gridOffset = new Vector3((xSize * cellSize) / 2f, (ySize * cellSize) / 2f, 0);
        int i = 0;
        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                posArray[i] = new Vector3((x * cellSize)-gridOffset.x, (y * cellSize*-1f) + gridOffset.y);
                //Spawn Tiles
                GameObject temp  = SpawnAt(tilePrefab,posArray[i], Quaternion.identity);
                
                if (temp)
                    temp.transform.parent = gameObject.transform;
                i++;
            }
        }

        for (int a = 0; a < gameElementList.Count; a++)
        {
            //Spawn Obstacles
            //Get the type
            GameElementModel gEM = GetGameElementPrefab(gameElementList[a].eType);
            if (gEM == null)
            {
                return;
            }
            //get the prefab to instatiate
            GameObject obstructPrefab = gEM.prefabObject;
            if (obstructPrefab)
            {
                //spawn prefab clone
                GameObject temp = SpawnAt(obstructPrefab, posArray[gameElementList[a].positionIndex], Quaternion.identity);
                Transform tempTransform = gameObject.transform.GetChild(gameElementList[a].positionIndex);
                if (tempTransform)
                {
                    //copy availabilty of tile over
                    TileTrigger tt = tempTransform.GetComponentInChildren<TileTrigger>();
                    if (tt)
                    {
                        if (tt)
                        {
                            tt.available = !gEM.obstacle;//set the availability to the  same as set in the setup list.
                        }
                    }
                }
                temp.transform.parent = gameObject.transform;

                if((gEM.eType == eGameElementType.STARTPOINT) && myDrone)
                {
                    myDrone.transform.position = temp.transform.position;
                }
                if ((gEM.eType == eGameElementType.EXITPOINT) && myDrone)
                {
                    myDrone.exitPoint = temp.transform.position;
                }
            }
        }
    }


    private GameElementModel GetGameElementPrefab(eGameElementType eType)
    {
        return gameElements.gameElement[(int)eType];
    }

    public GameObject SpawnAt(GameObject go, Vector3 pos, Quaternion orientation)
    {
        GameObject goInstance = Instantiate(go, pos, orientation);
        if (goInstance)
        {
            return goInstance;
        }
        return null;
    }

    internal void MoveDroneHere(TileTrigger tileTrigger)
    {
        Vector3 direction = tileTrigger.transform.position - myDrone.transform.position;

        myDrone.Hover(direction.normalized, direction.magnitude);
    }
}
