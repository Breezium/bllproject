using UnityEngine;

public class TileTrigger : MonoBehaviour
{
    Renderer [] myRenderers;
    public bool available;
    public bool outOfRange;
    
    // Start is called before the first frame update
    void Start()
    {
        myRenderers = GetComponentsInChildren<Renderer>();
        if (!available)
        {
            SetRenderers(Color.red);
        }
    }

    private void SetRenderers(Color col)
    {
        if (myRenderers == null)
            return;
        for (int i = 0; i < myRenderers.Length; i++)
        {
            myRenderers[i].material.color = col;
        }
    }

    private void OnMouseOver()
    {
        if (!available)
        {
            SetRenderers(Color.red);
            return;
        }
        outOfRange = (Grid.GetGrid().myDrone.transform.position - transform.position).magnitude > Grid.GetGrid().droneRange;
        if (outOfRange)
            return;
        if (!Grid.GetGrid().myDrone.isMoving)
        {
            SetRenderers(Color.green);
            return;
        }
        SetRenderers(Color.yellow);
    }
    private void OnMouseExit()
    {
        SetRenderers( Color.white);
    }

    private void OnMouseUpAsButton()
    {
        if (!Grid.GetGrid().myDrone.isMoving && available && !outOfRange)
        {
            Grid.GetGrid().MoveDroneHere(this);

        }
    }
}
